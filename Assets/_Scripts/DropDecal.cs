﻿using UnityEngine;
using System.Collections;

public class DropDecal : MonoBehaviour {
    public GameObject DecalObject;
    public Material[] Decals;

    public void Drop_Decal(Collision col)
    {
        int size = Decals.Length-1;
        int target = Random.Range(0, size);
        GameObject decalObj = Instantiate(DecalObject, col.contacts[0].point, Quaternion.identity) as GameObject;
        decalObj.GetComponent<Renderer>().material = Decals[target];
        decalObj.transform.SetParent(col.transform);
        decalObj.transform.Find("CFXM4 Hit Paint A (Cyan)").GetComponent<ParticleSystem>().startColor = Decals[target].color;
        decalObj.transform.Find("CFXM4 Splats").GetComponent<ParticleSystem>().startColor = Decals[target].color;
        var rot = decalObj.transform.localEulerAngles;
        rot.y = Random.Range(0, 360);
        decalObj.transform.localScale = new Vector3(2.0f,2.0f,2.0f);
        decalObj.GetComponent<Renderer>().materials[0].color = ElementsColor.ballColor;
    }

    public void SetDecalColors(Color color)
    {
        for (int i = 0; i < Decals.Length; i++)
        {
            Decals[i].color = color;
        }
    }
}
