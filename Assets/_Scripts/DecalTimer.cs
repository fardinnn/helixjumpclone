﻿using UnityEngine;
using System.Collections;

public class DecalTimer : MonoBehaviour
{
    float timer = 0;
    void Start()
    {
        timer = 0;
    }

	// Update is called once per frame
	void FixedUpdate () {
        timer += Time.fixedDeltaTime;
        if (timer > 2) 
        {
            Destroy(this.gameObject);
        }
	}
}
