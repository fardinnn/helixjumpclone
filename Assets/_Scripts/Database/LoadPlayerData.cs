﻿using UnityEngine;
using System.Collections;

public class LoadPlayerData : MonoBehaviour {

    public static int GetID(string tableName)
    {
		int id = PlayerPrefs.GetInt("LastID",0)+1;
		PlayerPrefs.SetInt("LastID",id);
        return id;
    }

    public static void Load(Player player)
    {
        player.id = PlayerPrefs.GetInt("LastID", 1);
        player.name = PlayerPrefs.GetString("playerName", "name");
        player.record = PlayerPrefs.GetInt(player.name + "_record", 0);
        player.level = PlayerPrefs.GetInt(player.name + "_LevelNum", 1);
        player.score = PlayerPrefs.GetInt(player.name + "_score", 0);
        //Debug.Log(player.id);
        //Debug.Log(player.name);
        //Debug.Log(player.record);
        //Debug.Log(player.level);
        //Debug.Log(player.score);
    }
}
