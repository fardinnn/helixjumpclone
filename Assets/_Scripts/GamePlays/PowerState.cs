﻿using UnityEngine;
using System.Collections;

public class PowerState : MonoBehaviour {
    public static bool isActive;
    static ParticleSystem ballFlame;
    static Color flameColor;
    static float ballSpeed;
    public static void Run(float speed){
        ballSpeed = speed;
        if (ballFlame==null)
        {
            ballFlame = GameObject.FindGameObjectWithTag("Player").transform.Find("particleObject").Find("CFX4 Flamme").GetComponent<ParticleSystem>();
            flameColor = ballFlame.startColor;
        }
        flameColor.a = (speed-200)/(AddPhysics.MaxSpeed - 200.0f);
        ballFlame.startColor = flameColor;
        if (speed > 500.0f)
        {
            //Debug.Log("speed:" + speed);
            isActive = true;
        }
    }

    public static void Impact()
    {
        //Debug.Log("ballSpeed: " + ballSpeed);

    }
}
