﻿using UnityEngine;
using System.Collections;

public class Geometry : MonoBehaviour {
    public GameObject ball;
    public GameObject tail;
    public GameObject fireTail;

    static float acceleration;
    static float changeValue;
    static float lastSpeed = 0;
    static Vector3 lastScale = new Vector3(10.0f,10.0f,10.0f);

    public void SetColor(Color color)
    {
        color.a = 0.4f;
        ball.GetComponent<Renderer>().material.color = color;
        tail.GetComponent<ParticleSystem>().startColor = color;
        //fireTail.GetComponent<ParticleSystem>().startColor = color;
    }

    public static Vector3 SetBounceScale(float speed,float deltaTime)
    {
        acceleration = (speed - lastSpeed) / deltaTime;
        lastSpeed = speed;
        changeValue = Mathf.SmoothStep(changeValue, acceleration / 20.0f, 0.1f);
        lastScale.x = 10 * changeValue/200;
        lastScale.y = 10 / (changeValue * changeValue/40000);
        lastScale.z = 10 * changeValue/200;

        return new Vector3(10.0f,10.0f,10.0f);
    }
}