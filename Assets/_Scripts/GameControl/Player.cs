﻿using UnityEngine;
using System.Collections;

public class Player {
    public int id
    {
        get;
        set;
    }
    public string name
    {
        get;
        set;
    }
    public int score
    {
        get
        {
            return Score.GetScore;
        }
        set
        {
            Score.GetScore = value;
        }
    }
    public int record
    {
        get
        {
            return Score.Record;
        }
        set
        {
            Score.Record = value;
        }
    }
    public int level
    {
        get;
        set;
    }
}
