﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour {
    static Level currentLevel;
    Score score;
    ColumnControl columnControl;
    CameraControl cameraControl = null;
	// Use this for initialization
    void Awake()
    {
        currentLevel = Level.CurrentLevel;
        currentLevel.Load();
        columnControl = GameObject.FindGameObjectWithTag("Column").GetComponent<ColumnControl>();
        cameraControl = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>();
	}

    void Start()
    {
        Restart_Level();
    }
    public void Restart_Level()
    {
        //Debug.Log("RestartLevel");
        ElementsColor.RandomSet();
        currentLevel.StartLevel(Level.LevelNum);
        columnControl.StartColumns();
        GameObject.FindGameObjectWithTag("Player").GetComponent<AddPhysics>().resetBall();
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>().ResetCamera();
    }
    public void Start_Level()
    {
        ElementsColor.RandomSet();
        currentLevel.StartLevel(++Level.LevelNum);
        columnControl.StartColumns();
        GameObject.FindGameObjectWithTag("Player").GetComponent<AddPhysics>().resetBall();
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>().ResetCamera();
    }

    public void On_Skip_Floor()
    {

        if (score == null)
        {
            score = GameObject.FindGameObjectWithTag("Controller").GetComponent<Score>();
        }
        currentLevel.SkippedFloor++;
        Difficulty.CurrentDifficulty(currentLevel.RelativePosition);
        if (currentLevel.RelativePosition < 1)
        {
            cameraControl.Move_To_Next(true);
            columnControl.Generatefloors(ElementsColor.safeColor , ElementsColor.badColor);
        }
        else if (currentLevel.RelativePosition == 1)
        {
            columnControl.GenerateBottomBase(ElementsColor.centerColor);
        }
        else
        {
            if (ColumnControl.floors.Count < 2)
                cameraControl.Move_To_Next(false);

        }
    }
}