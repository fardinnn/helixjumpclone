﻿using UnityEngine;
using System.Collections;

public class LostLevel : MonoBehaviour {

    static GameObject LostUI;
    public static bool lostMode;
    
    public static void Lost(int LevelNum)
    {
        if (LostUI == null)
        {
            LostUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("Lost").gameObject;
        }
        if (LostUI.activeSelf)
            return;
        LostUI.SetActive(true);
        LostUI.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Lost";
        LostUI.GetComponent<Animator>().SetBool("Play", true);
        lostMode = true;
        SavePlayerData.Save(Level.activePlayer);
    }


    public void On_Restart_Level()
    {
        lostMode = false;
        LostUI.SetActive(false);
        Score.GetScore = 0;
        GameObject.FindGameObjectWithTag("Controller").GetComponent<GameControl>().Restart_Level();
    }
}
