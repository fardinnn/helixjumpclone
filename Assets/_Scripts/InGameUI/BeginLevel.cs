﻿using UnityEngine;
using System.Collections;
public class BeginLevel : MonoBehaviour {


    static GameObject beginLevelUI;

    public static void Begin(int levelNum)
    {
        if (beginLevelUI==null)
            beginLevelUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("BeginLevel").gameObject;
        beginLevelUI.SetActive(true);
        beginLevelUI.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Level " + levelNum + " begins";
        beginLevelUI.GetComponent<Animator>().SetBool("Play", true);
    }

    public void On_Animation_End()
    {
        GetComponent<Animator>().SetBool("Play", false);
        transform.gameObject.SetActive(false);
    }
}
