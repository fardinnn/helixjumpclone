﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour
{
    static GameObject FinishUI;
    public static void End(int LevelNum)
    {
        if (FinishUI==null)
        {
            FinishUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("LevelFinish").gameObject;
        }
        FinishUI.SetActive(true);
        FinishUI.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Level " + LevelNum + " passed";
        FinishUI.GetComponent<Animator>().SetBool("Play", true);
        SavePlayerData.Save(Level.activePlayer);
    }
}
