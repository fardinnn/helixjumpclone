﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
    Vector3 cameraPos = Vector3.zero;
    Vector3 newCamPos = Vector3.zero;
    public float relativeHeiht;
    float timer = 0;
    ColumnControl columnControl;
    Transform player;
    [HideInInspector]
    public float startHeight;
    Vector3 startPos;

    bool moveToNext = true;
    void Awake()
    {
        columnControl = GameObject.FindGameObjectWithTag("Column").GetComponent<ColumnControl>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        startPos = transform.position;
        ResetCamera();
    }

    void FixedUpdate()
    {
        if (StartMenu.gameBegan)
        {
            if (moveToNext)
            {
                newCamPos.y = cameraPos.y - player.position.y;
                cameraPos.y += (-newCamPos.y + relativeHeiht) < 0 ? relativeHeiht - newCamPos.y : 0;
            }
            transform.position = cameraPos;
            if ((startHeight-cameraPos.y)>472.4f)
            {
                startHeight = cameraPos.y;
                ColumnControl.RelocateCenter();
            }
        }
    }
    public void Move_To_Next(bool movable)
    {
        newCamPos = newCamPos + Vector3.up * -columnControl.floorDistance;
        moveToNext = movable;
        timer = 0;
    }

    public void ResetCamera()
    {

        transform.position = startPos;
        resetPosition();
        startHeight = transform.position.y;
        moveToNext = true;
    }

    public void resetPosition(){
        cameraPos = transform.position;
        newCamPos = transform.position;
    }
}