﻿using UnityEngine;
using System.Collections;

public class ShapeDifficulties : MonoBehaviour {

    public static int MaxIndex(int maxDifficulty)
    {
        int shapeDifficulty = 0;
        for (int i = 1; i < 10; i++)
        {
            if (maxDifficulty<i * 3)
            {
                shapeDifficulty = i-1;
                break;
            }
        }
        if (maxDifficulty>30)
        {
            shapeDifficulty = 8;
        }
        return shapeDifficulty;
    }
}
