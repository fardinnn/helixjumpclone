﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {
    static float startScore;
    static int levelNum;

    static void Load()
    {
        ResetFloors();
        SetParameters();
        ResetUIs();
    }
    static void SetParameters()
    {
        Difficulty.SetLevelDifficulty(levelNum);
    }
    
    static void ResetFloors()
    {

    }

    static void ResetUIs()
    {

    }
}
