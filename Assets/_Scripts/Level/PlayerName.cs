﻿using UnityEngine;
using System.Collections;

public class PlayerName : MonoBehaviour {
    [SerializeField]
    UnityEngine.UI.InputField nameField;
    [SerializeField]
    UnityEngine.UI.Button ApproveButton;


    public void SetPlayerName()
    {
        PlayerPrefs.SetString("playerName", nameField.text);
        PlayerPrefs.SetInt(nameField.text + "_id", LoadPlayerData.GetID("Records"));
        Level.CurrentLevel.Load();
        gameObject.SetActive(false);
    }

    public void OnNameChange()
    {
        if (nameField.text.Length>0)
        {
            ApproveButton.interactable = true;
        }
        else
        {
            ApproveButton.interactable = false;
        }
    }
}
