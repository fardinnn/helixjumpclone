﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level : MonoBehaviour {

    static Slider progressBar = null;
    static Text currenLevelUI;
    static Text nextLevelUI;
    static string playerName;

    private void Awake()
    {
        progressBar = GameObject.FindGameObjectWithTag("Canvas").transform.Find("HUD").
            Find("Progress").GetComponent<Slider>();
        currenLevelUI = progressBar.transform.Find("CurrentLevel").Find("Text").GetComponent<Text>();
        nextLevelUI = progressBar.transform.Find("NextLevel").Find("Text").GetComponent<Text>();
    }

    int _skippedFloor;
    public int SkippedFloor
    {
        get
        {
            return _skippedFloor;
        }
        set
        {
            _skippedFloor = value;
            RelativePosition = _skippedFloor / (numberOfFloors * 1.0f);
        }
    }

    private static Level level = null;
    private static readonly object locker = new object();
    int numberOfFloors;
    bool isActive;
    public static int score;
    int maxScore;
    public static Player activePlayer;
    static int levelNum;

    public static int LevelNum
    {
        get
        {
            return levelNum;
        }
        set
        {
            //ElementsColor.RandomSet();
            levelNum = value;
            activePlayer.level = levelNum;
            currenLevelUI.text = "" + levelNum;
            nextLevelUI.text = "" + (levelNum + 1);
        }
    }

    float relativePosition = 0.0f;

    public float RelativePosition
    {
        get
        {
            return relativePosition;
        }
        set
        {
            relativePosition = value;
            progressBar.value = relativePosition;
        }
    }

    public static bool levelWin;


    public static Level CurrentLevel
    {
        get
        {
            lock (locker)
            {
                if (level==null)
                {
                    level = new GameObject("Level").AddComponent<Level>();
                }
                return level;
            }
        }
    }

    public void Load()
    {
        activePlayer = new Player();
        if (!GetPlayerName())
            return;
        LoadData();
        Begin();
        RelativePosition = 0.0f;
    }
    bool GetPlayerName()
    {
        playerName = PlayerPrefs.GetString("playerName", "");
        Level.activePlayer.name = playerName;
        if (playerName=="")
        {
            GameObject.FindGameObjectWithTag("Canvas").transform.Find("PlayerName").gameObject.SetActive(true);
            return false;
        }

        activePlayer.name = playerName;
        activePlayer.id = PlayerPrefs.GetInt(playerName + "_id", LoadPlayerData.GetID("Records"));
        return true;
    }
    void LoadData()
    {
        LoadPlayerData.Load(Level.activePlayer);
        LevelNum = Level.activePlayer.level;
        PlayerPrefs.SetInt("LastID", Level.activePlayer.id);
    }

    public void Save()
    {
        PlayerPrefs.SetInt(playerName+"_LevelNum", LevelNum);
    }

    /*public void LoadLevel(int number)
    {
        LevelNum = number;
        Debug.Log("LevelNum: " + LevelNum);
        Begin();
    }*/

    void Begin()
    {
        levelWin = false;
        SetFloorsCount();
    }

    void SetFloorsCount()
    {
        numberOfFloors = 5 + LevelNum;
    }

    public void StartLevel(int Number)
    {
        levelWin = false;
        SkippedFloor = 0;
        LevelNum = Number;
        Difficulty.SetLevelDifficulty(LevelNum);
        SetFloorsCount();
        Save();
        BeginLevel.Begin(Number);
    }
}
