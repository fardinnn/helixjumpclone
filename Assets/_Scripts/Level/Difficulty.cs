﻿using UnityEngine;
using System.Collections;

public class Difficulty : MonoBehaviour {

    static float maxDifficulty = 2;
    public static float currentDifficulty = 0;

    [HideInInspector]
    public static int easy;
    [HideInInspector]
    public static int hard;

    public static void SetLevelDifficulty(int levelNum)
    {
        maxDifficulty = levelNum + 2;
        easy = 0;
        hard = 1;
    }

    public static float GetMaxDifficulty()
    {
        return maxDifficulty;
    }

    public static float GetCurrentDifficulty()
    {
        return currentDifficulty;
    }

    public static float CurrentDifficulty(float relativePosition)
    {
        //Debug.Log("relativePosition:" + relativePosition + "    : maxDifficulty: " + ShapeDifficulties.MaxIndex((int)maxDifficulty));
        currentDifficulty = Mathf.Log((Mathf.Pow(relativePosition * 2.56f, maxDifficulty) + 1));
        easy = (int)Mathf.Floor((currentDifficulty / maxDifficulty) * ShapeDifficulties.MaxIndex((int)maxDifficulty));//(int)(levelExp / 20.0f);
        //Debug.Log("levelExp: " + levelExp + " easy: " + easy);
        hard = easy + 3;
        //Debug.Log("easy:" + easy + " : hard: " + hard);
        if (hard > 8)
        {
            easy = 6;
            hard = 8;
        }
        //Debug.Log("currentDifficulty:" + currentDifficulty + "   : " + easy + "  :  " + hard + "  :  " + ShapeDifficulties.MaxIndex((int)maxDifficulty));
        return currentDifficulty;
    }

}