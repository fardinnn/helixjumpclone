﻿using UnityEngine;
using System.Collections;

public class MoveEverything : MonoBehaviour {
    static Transform mainCam;
    static Transform player;
    public static void Moving(float distance)
    {
        Vector3 savePosition;
        if (player==null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
            mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        }
        savePosition = player.transform.localPosition;
        savePosition.y += distance;
        player.transform.localPosition = savePosition;

        savePosition = mainCam.transform.localPosition;
        savePosition.y += distance;
        mainCam.transform.localPosition = savePosition;
        mainCam.GetComponent<CameraControl>().resetPosition();
        mainCam.GetComponent<CameraControl>().startHeight += distance;
        ColumnControl.ResetPositions(distance);
    }
}