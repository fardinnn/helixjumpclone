﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class ElementsColor : MonoBehaviour {
    static Renderer playerRenderer;

    public static Color ballColor;
    public static Color badColor;
    public static Color safeColor;
    public static Color centerColor;
    public static Color baseColor;

    static int colorNumbers = 5;
    static Color[] ballColors = {
                                    Color.red, new Color(1.0f,185/255.0f,0,1.0f),
                                    Color.blue, Color.black,
                                    new Color(0.44f,0.0f,1.0f,1.0f)
                               };
    static Color[] dangerColors = {
                                    Color.red, new Color(0.9f,1.0f,0.0f,1.0f), new Color(0.0f,0.0f,0.4f,1.0f),
                                    Color.black, new Color(0.06f,0.376f,0.0f,1.0f), 
                                    new Color(0.44f,0.0f,1.0f,1.0f)
                                  };
    static Color[] goodColors = {
                                    new Color(1.0f,0.0f,1.0f,1.0f), Color.yellow,
                                    new Color(0.0f,1.0f,1.0f,1.0f), Color.green, 
                                    new Color(0.44f,0.0f,1.0f,1.0f)
                                };
    static Color[] CenterColors = {
                                     new Color(1.0f,0.0f,1.0f,1.0f), Color.yellow, 
                                     new Color(0.0f,1.0f,1.0f,1.0f), Color.green,
                                    new Color(0.44f,0.0f,1.0f,1.0f)
                                };
    static HashSet<int> usedColors;
    static List<int> restOfColors;
    public static void RandomSet()
    {
        int targetindex = Random.Range(0, ballColors.Length - 1);
        ballColor = ballColors[targetindex];
        playerRenderer = GameObject.FindGameObjectWithTag("Player").GetComponent<Renderer>();

        usedColors = new HashSet<int>();
        restOfColors = new List<int>();
        usedColors.Add(targetindex);
        badColor = dangerColors[NextIndex(ref targetindex)];
        safeColor = goodColors[NextIndex(ref targetindex)];
        centerColor = CenterColors[NextIndex(ref targetindex)];
        baseColor = CenterColors[NextIndex(ref targetindex)];
        SetColors();
    }

    public static void SetColors()
    {
        GameObject.FindGameObjectWithTag("Controller").GetComponent<DropDecal>().SetDecalColors(ballColor);
        GameObject.FindGameObjectWithTag("Player").GetComponent<Geometry>().SetColor(ballColor);
        var columnControl = GameObject.FindGameObjectWithTag("Column").GetComponent<ColumnControl>();
        columnControl.badMat.color = badColor;
        columnControl.safeMat.color = safeColor;
        columnControl.centerMat.color = centerColor;
        columnControl.baseMat.color = baseColor;
    }

    static int NextIndex(ref int index)
    {
        usedColors.Add(index);

        restOfColors = new List<int>();
        for (int i = 0; i < colorNumbers; i++)
        {
            if (!usedColors.Contains(i))
            {
                restOfColors.Add(i);
            }
        }
        index = restOfColors.ElementAt(Random.Range(0, restOfColors.Count - 1));
        return index;
    }
}