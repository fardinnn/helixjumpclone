﻿using UnityEngine;
using System.Collections;

public class SkipFloor : MonoBehaviour {
    public static int continuousSkip;
    bool oneTimeRun = false;
    GameControl gameControl = null;
    ColumnControl columnControl = null;
    void Awake()
    {
        gameControl = GameObject.FindGameObjectWithTag("Controller").GetComponent<GameControl>();
        columnControl = GameObject.FindGameObjectWithTag("Column").GetComponent<ColumnControl>();
    }

    public void skip(GameObject skipFloor)
    {
        if (!oneTimeRun)
        {
            continuousSkip++;
            gameControl.On_Skip_Floor();
            columnControl.DestroyFloor(skipFloor);

            oneTimeRun = true;
            StartCoroutine(keepTillEndOfFrame());
        }
    }

    IEnumerator keepTillEndOfFrame()
    {
        yield return new WaitForEndOfFrame();
        oneTimeRun = false;
    }
}
