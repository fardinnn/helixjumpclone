﻿using UnityEngine;
using System.Collections;

public class AudioPlay : MonoBehaviour {
    AudioSource audio = null;
	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        if (audio==null)
            return;
        float pressure = (SkipFloor.continuousSkip - 1.0f) / (0.1f + SkipFloor.continuousSkip);
        audio.pitch = 0.875f + pressure * pressure + Difficulty.GetCurrentDifficulty() / Difficulty.GetMaxDifficulty();
        audio.Play();
	}
}
