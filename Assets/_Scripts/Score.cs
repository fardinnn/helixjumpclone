﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    static Text scoreBoard = null;
    static Text RecordBoard = null;

    public static int _record = 0;
    static int _score;
    static float difficultyAverage = 0; 
    public static int GetScore
    {
        get
        {
            return _score;
        }
        set
        {
            On_Update_Score(value);
            scoreBoard.text = "" + value;
        }
    }

    public static int Record
    {
        get
        {
            return _record;
        }
        set
        {
            _record = value;
            RecordBoard.text = "" + value;
        }
    }


	// Use this for initialization
	void Awake () {
        scoreBoard = GameObject.FindGameObjectWithTag("Canvas").transform.Find("HUD").
            Find("ScoreBoard").GetComponent<Text>();
        RecordBoard = GameObject.FindGameObjectWithTag("Canvas").transform.Find("HUD").
            Find("Record").GetComponent<Text>();
    }

    public static void On_Update_Score()
    {
        difficultyAverage = (Difficulty.easy+Difficulty.hard)/2.0f;
        Level.activePlayer.score += 10 * (SkipFloor.continuousSkip / 3) + 5 * (int)difficultyAverage;
        if (SkipFloor.continuousSkip>2)
        {
            //Debug.Log("::: " + (10 * SkipFloor.continuousSkip * (Difficulty.currentDifficulty + 1)) +"  :  "+ Record);
			if ((10 * SkipFloor.continuousSkip * (Difficulty.currentDifficulty+1)) > Record)
            {
                Level.activePlayer.record = (int)(10 * SkipFloor.continuousSkip * (Difficulty.currentDifficulty + 1));

                //Debug.Log("Level.activePlayer.record: " + Level.activePlayer.record + " : " + Record);
                //Debug.Log("Record: " + Record); 
            }
        }
    }

    public static void On_Update_Score(int score)
    {
        _score = score;
        scoreBoard.text = "" + _score;
    }
}
