﻿using UnityEngine;
using System.Collections;

public class AboatUS : MonoBehaviour {
    bool isActive = false;
	// Use this for initialization
	void FixedUpdate () {
        if (!isActive)
        {
            isActive = true;
            StartCoroutine(hide());
        }
	}
	
    IEnumerator hide(){
        yield return new WaitForSeconds(4);
        isActive = false;
        gameObject.SetActive(false);
    }
}