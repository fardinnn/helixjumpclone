﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {
    public static bool gameBegan = false;
    Animator cameraAnim = null;
    public void StartGame()
    {
        StartCoroutine(SetCameraAnimation());
    }

    IEnumerator SetCameraAnimation()
    {
        cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").transform.parent.GetComponent<Animator>();
        cameraAnim.SetBool("StartGame", true);
        yield return new WaitForSeconds(0.2f);
        gameObject.SetActive(false);
        gameBegan = true;
        Destroy(ColumnControl.startingFloor);
    }
}
