﻿using UnityEngine;
using System.Collections;

public class AddPhysics : MonoBehaviour {

    Vector3 velocity;
    DropDecal dropDecal;
    [SerializeField]
    Transform particleObject;

    public static float MaxSpeed = 600.0f;
    Vector3 startPos;
	// Use this for initialization
	void Awake () {
        dropDecal = GameObject.FindGameObjectWithTag("Controller").GetComponent<DropDecal>();
        startPos = transform.position;
	}

    public void resetBall()
    {
        transform.position = startPos;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
	// Update is called once per frame
	void FixedUpdate () {
        PowerState.Run(Mathf.Abs(GetComponent<Rigidbody>().velocity.y));
        velocity = GetComponent<Rigidbody>().velocity;
        transform.localScale = Geometry.SetBounceScale(velocity.y, Time.fixedDeltaTime);
        SetParticleTrail(velocity);
        if (Mathf.Abs(velocity.y) >= MaxSpeed && GetComponent<Rigidbody>().useGravity)
        {
            GetComponent<Rigidbody>().useGravity = false;
            //velocity.y = Mathf.Sign(velocity.y) * MaxSpeed;
            //rigidbody.velocity = velocity;
        }
        else if (Mathf.Abs(velocity.y) < MaxSpeed && !GetComponent<Rigidbody>().useGravity)
        {
            GetComponent<Rigidbody>().useGravity = true;
        }
        if (transform.position.y < -10000)
        {
            MoveEverything.Moving(-transform.position.y);
        }
        if (GetComponent<Rigidbody>().useGravity)
        {
            GetComponent<Rigidbody>().AddForce(0.0f, -490.19f, 0.0f);
        }
	}

    void OnCollisionEnter(Collision col)
    {
        
        //if (col.collider.tag == "Safe")
        //{
        //Debug.Log("continuousSkip hitted");
        SkipFloor.continuousSkip = 0;
        if (PowerState.isActive && col.collider.tag != "Finish")
        {
            PowerState.Impact();
            GetComponent<SkipFloor>().skip(col.collider.transform.parent.gameObject);
            GetComponent<Rigidbody>().velocity = new Vector3(0, 200, 0);
            return;
        }
        PowerState.isActive = false;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 200, 0);
        dropDecal.Drop_Decal(col);
        //}
        if (col.collider.tag == "Finish")
        {
            if (!Level.levelWin)
            {
                Level.levelWin = true;
                EndLevel.End(Level.LevelNum);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        //Debug.Log("col.tag:" + col.tag + " : " + PowerState.isActive);
        if (col.GetComponent<Collider>().tag == "RedFloor")
        {
            if (!PowerState.isActive)
            {
                PowerState.isActive = false;
                LostLevel.Lost(Level.LevelNum);
            }
            else
            {
                PowerState.isActive = false;
                GetComponent<Rigidbody>().velocity = new Vector3(0, 200, 0);
                GetComponent<SkipFloor>().skip(col.transform.parent.gameObject);
            }
            SkipFloor.continuousSkip = 0;
            //lose state
        }
        else if (col.tag == "NextFloor")
        {
            Score.On_Update_Score();
            GetComponent<SkipFloor>().skip(col.transform.parent.gameObject);
        }
    }



    void SetParticleTrail(Vector3 velocity)
    {
        particleObject.eulerAngles = new Vector3((-(velocity.y / MaxSpeed) - 1) * -90, 0, 0);
    }
}
