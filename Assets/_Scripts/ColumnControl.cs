﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColumnControl : MonoBehaviour {
    AddElements addElements;
    public static GameObject[] centers;
    public static HashSet<GameObject> floors;
    public static GameObject startingFloor;
    public float floorDistance = 100;
    public float centerStart = 50;
    static float centerPos;
    static int activeCenter;
    static float startFloor;
    static float floorPos;
    static float startAngle;
    static float startMousePos;
    public float Sensitivity = 1.0f;
    public Material centerMat;
    public Material baseMat;
    public Material safeMat;
    public Material badMat;

	// Use this for initialization
    public void StartColumns()
    {
        //Debug.Log("aaaa");
        ResetAll();
        centerPos = centerStart;
        startFloor = floorDistance;
        floorPos = floorDistance;
        addElements = GameObject.FindGameObjectWithTag("Controller").GetComponent<AddElements>();
        OnStartGenerate();
	}
	

    void ResetAll()
    {
        if (centers ==null)
        {
            centers = new GameObject[2];
            floors = new HashSet<GameObject>();
            return;
        }

        for (int i = centers.Length-1; i >= 0; i--)
        {
            Destroy(centers[i]);
        }

        foreach (var floor in floors)
        {
            Destroy(floor);
        }
        centers = new GameObject[2];
        floors = new HashSet<GameObject>();
    }

    void OnStartGenerate()
    {
        GenerateStartFloor(ElementsColor.centerColor);
        GenerateCenter(ElementsColor.centerColor);
        for (int i = 0; i < 3; i++)
            Generatefloors(ElementsColor.safeColor, ElementsColor.badColor);
 
    }

    public void GenerateCenter(Color color)
    {
        for (int i = 0; i < 2; i++)
        {
        centers[i] = Instantiate(addElements.centerColumn, Vector3.zero, Quaternion.identity) as GameObject;
        centers[i].transform.SetParent(transform);
        centers[i].transform.localEulerAngles = new Vector3(-90, 0, 0);
        centerPos -= 472.4f;
        centers[i].transform.position = new Vector3(0, centerPos, 0);
        activeCenter = 1;
        }
    }

    public static void RelocateCenter()
    {
        activeCenter = activeCenter == 0 ? 1 : 0;
        centerPos -= 472.4f;
        centers[activeCenter].transform.position = new Vector3(0, centerPos, 0);
    }

    public void GenerateStartFloor(Color color)
    {
        if (!StartMenu.gameBegan)
        {
            startingFloor = Instantiate(addElements.startingFloor, Vector3.zero, Quaternion.identity) as GameObject;
            startingFloor.transform.SetParent(transform);
            startingFloor.transform.localEulerAngles = new Vector3(-90, 0, 0);
            floorPos -= floorDistance;
            startingFloor.transform.position = new Vector3(0, floorPos, 0);
        }
    }

    public void Generatefloors(Color safeColor, Color badColor)
    {
        int index = Random.Range(Difficulty.easy, Difficulty.hard);
        GameObject newObj = Instantiate(addElements.elements[index], Vector3.zero, Quaternion.identity) as GameObject;
        newObj.transform.SetParent(transform);
        newObj.transform.localEulerAngles = new Vector3(-90, Random.Range(0,360),0);
        floorPos -= floorDistance;
        newObj.transform.position = new Vector3(0, floorPos, 0);
        floors.Add(newObj);
    }

    public void GenerateBottomBase(Color color)
    {
        GameObject newObj = Instantiate(addElements.baseElement, Vector3.zero, Quaternion.identity) as GameObject;
        newObj.transform.SetParent(transform);
        newObj.transform.localEulerAngles = new Vector3(-90, Random.Range(0,360),0);
        floorPos -= floorDistance;
        newObj.transform.position = new Vector3(0, floorPos, 0);
        floors.Add(newObj);
    }

    public void DestroyFloor(GameObject floor){
        if (floors.Contains(floor))
        {
            floors.Remove(floor);
            StartCoroutine(ExplodeFloor(floor));
        }
    }

    IEnumerator ExplodeFloor(GameObject floor)
    {
        floor.GetComponent<Animator>().SetBool("play",true);
        yield return new WaitForSeconds(0.5f);
        Destroy(floor);
    }

    public void On_Begin_Drag_Rotate()
    {
        startAngle = transform.localEulerAngles.y;
        startMousePos = Input.mousePosition.x;
    }

    public void On_Drag_Rotate()
    {

        if (!LostLevel.lostMode && !Level.levelWin)
        {
            transform.localEulerAngles = new Vector3(0, startAngle - (Input.mousePosition.x - startMousePos) * Sensitivity, 0);
        }
    }

    public static void ResetPositions(float distance)
    {
        Vector3 savePosition;
        foreach (var item in centers)
        {
            savePosition = item.transform.localPosition;
            savePosition.y += distance;
            item.transform.localPosition = savePosition;
        }
        foreach (var item in floors)
        {
            savePosition = item.transform.localPosition;
            savePosition.y += distance;
            item.transform.localPosition = savePosition;
        }
        centerPos = 0;
        floorPos = 0;
    }


}